const fileJson = require("../fileJson");

class User {
  constructor(id, login, role, fullname, registeredAt, avaUrl, isDisabled) {
    this.id = id;
    this.login = login;
    this.role = role;
    this.fullname = fullname;
    this.registeredAt = registeredAt;
    this.avaUrl = avaUrl;
    this.isDisabled = isDisabled;
  }

  static getAll(callback) {
    fileJson.parseJSON("/data/users.json", (err, data) => {
      if (err) callback(err);
      else callback(null, data.items);
    });
  }

  static getById(id, callback) {
    if (typeof id !== "number") {
      id = parseInt(id);
    }
    fileJson.parseJSON("/data/users.json", (err, data) => {
      if (err) callback(err);
      else {
        const user = data.items.find(x => id === x.id);
        if (user === undefined) callback("User doesn't exist");
        else callback(null, user);
      }
    });
  }
}

module.exports = User;
