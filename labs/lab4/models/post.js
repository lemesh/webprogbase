const fileJson = require("../fileJson.js");

function update(obj) {
  fileJson.jsonStringifyToFile("/data/posts.json", obj, err => {
    if (err) console.log(err);
  });
}

class Post {
  constructor(id, content, title, userId, likes, postDate, photoUrl) {
    this.id = id;
    this.content = content;
    this.title = title;
    this.userId = userId;
    this.postDate = postDate;
    this.likes = likes;
    this.photoUrl = photoUrl;
  }

  static insert(x, callback) {
    if (!x.photoUrl) x.photoUrl = "";
    fileJson.parseJSON("/data/posts.json", (err, data) => {
      if (err) callback(err);
      else {
        data.items.push(
          new Post(
            data.nextId++,
            x.content,
            x.title,
            parseInt(x.userId),
            0,
            new Date(),
            x.photoUrl
          )
        );
        update(data);
        callback(null, data.nextId - 1);
      }
    });
  }

  static getAll(callback) {
    fileJson.parseJSON("/data/posts.json", (err, data) => {
      if (err) callback(err);
      else callback(null, data.items);
    });
  }

  static getById(id, callback) {
    if (typeof id !== "number") id = parseInt(id);
    fileJson.parseJSON("/data/posts.json", (err, data) => {
      if (err) callback(err);
      else {
        const user = data.items.find(x => x.id === id);
        if (user === undefined) callback("No such user");
        callback(null, user);
      }
    });
  }

  static update(obj, callback) {
    if (typeof obj.id !== "number") obj.id = parseInt(obj.id);
    fileJson.parseJSON("/data/posts.json", (err, data) => {
      if (err) callback(err);
      else {
        let post = data.items.find(x => x.id === obj.id);
        post = obj.post;
        update(data);
        callback(null, true);
      }
    });
  }

  static delete(id, callback) {
    if (typeof id !== "number") id = parseInt(id);
    fileJson.parseJSON("/data/posts.json", (err, data) => {
      if (err) callback(err);
      else {
        data.items = data.items.filter(x => x.id !== id);
        update(data);
        callback(null, true);
      }
    });
  }

  static getRangedArray(rangeStart, search, callback) {
    if (typeof rangeStart !== "number") rangeStart = parseInt(rangeStart);
    fileJson.parseJSON("/data/posts.json", (err, data) => {
      if (err) callback(err);
      else {
        if (search !== "") {
          data.items = data.items.filter(x =>
            x.title.match(new RegExp(`${search}`, "gi"))
          );
        }
        const start = (rangeStart - 1) * 10;
        const end = start + 10;
        const pageCount = Math.ceil(data.items.length / 10);
        const rangeEnd =
          pageCount > 5 && pageCount - rangeStart >= 5
            ? rangeStart + 4
            : pageCount;
        const pages = [];
        for (
          let i =
            pageCount - rangeStart >= 5
              ? rangeStart
              : pageCount > 5
                ? pageCount - 4
                : 1;
          i <= rangeEnd;
          i++
        )
          pages.push({ num: i });
        let next = "";
        if (rangeStart === pageCount) next = "next";
        const posts = data.items.slice(start, end);
        callback(null, {
          posts,
          pages,
          prev: "",
          next,
          len: ""
        });
      }
    });
  }

  // static getSortedByName(search, callback) {}
}

module.exports = Post;
