const express = require("express");
const busBoyBodyParser = require("busboy-body-parser");
const mustache = require("mustache-express");
const path = require("path");
const fileJson = require("./fileJson.js");
const fs = require("fs");

const app = express();

const User = require("./models/user.js");
const Post = require("./models/post.js");

let lastPage = 1;
let lastSearch = "";

app.use(express.static("public"));
app.use(busBoyBodyParser());
// app.use(require("morgan")("tiny"));

app.set("views", path.join(__dirname, "views"));
app.set("view engine", "mst");
app.engine("mst", mustache(path.join(__dirname, "views/partials")));

app.get("/", (req, res) => {
  res.render("index", { title: "Look At!" });
});

app.get("/users", (req, res) => {
  User.getAll((err, data) => {
    if (err) res.send(err);
    else res.render("users", { users: data });
  });
});

app.get("/users/:id", (req, res) => {
  User.getById(req.params.id, (err, data) => {
    if (err) res.send(err);
    else res.render("user", data);
  });
});

app.get("/posts", (req, res) => {
  if (req.query.search) {
    lastSearch = req.query.search;
  }
  if (req.query.page) {
    if (req.query.page === "Previous")
      res.redirect(`/posts?page=${lastPage - 1}`);
    else if (req.query.page === "Next")
      res.redirect(`/posts?page=${lastPage + 1}`);
    else {
      Post.getRangedArray(parseInt(req.query.page), lastSearch, (err, data) => {
        if (err) res.send(err);
        else {
          if (parseInt(req.query.page) === 1) data.prev = "prev";
          if (data.posts.length === 0) data.len = "len";
          res.render("posts", Object.assign(data, { search: lastSearch }));
          lastPage = parseInt(req.query.page);
          lastSearch = "";
        }
      });
    }
  } else {
    res.redirect(`/posts?page=1`);
  }
});

app.get("/posts/:id(\\d+)", (req, res) => {
  Post.getById(req.params.id, (err, data) => {
    if (err) res.send(err);
    else res.render("post", data);
  });
});

app.get("/posts/new", (req, res) => {
  res.render("newPost");
});

app.post("/posts/new", (req, res, next) => {
  const { title, content, userId } = req.body;
  const photo = req.files.photoUrl;
  let pathFile;
  if (photo) {
    pathFile = path.join("/data/fs/", photo.name);
    fileJson.writeFile(".." + pathFile, photo.data, null, err => {
      if (err) res.send(err);
    });
  }
  Post.insert(
    {
      title,
      content,
      userId,
      photoUrl: pathFile
    },
    (err, data) => {
      if (err) res.send(err);
      res.redirect(`/posts/${data}`);
    }
  );
});

app.get("/about", (req, res) => {
  res.render("about");
});

app.get("/data/fs/:img", (req, res) => {
  const file = fs.ReadStream(req.url.slice(1));
  file.pipe(res);
});

app.post("/posts/:id(\\d+)", (req, res, next) => {
  Post.delete(req.params.id, (err, data) => {
    if (err) res.send(err);
    else res.redirect("/posts");
  });
});

const port = process.env.PORT || 3001;
app.listen(port, () => console.log(`Server started on port ${port}`));
