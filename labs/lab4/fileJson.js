const fs = require("fs");
const path = require("path");

class fileJson {
  static readFile(filepath, callback) {
    fs.readFile(path.join(__dirname, filepath), (err, data) => {
      if (err) {
        callback(err);
      } else {
        callback(null, data);
      }
    });
  }

  static writeFile(filepath, data, encoding, callback) {
    fs.writeFile(path.join(__dirname, filepath), data, encoding, err =>
      callback(err)
    );
  }

  static parseJSON(filepath, callback) {
    const obj = this.readFile(filepath, (err, data) => {
      try {
        const obj = JSON.parse(data);
        callback(null, obj);
      } catch (e) {
        callback(e);
      }
    });
  }

  static jsonStringifyToFile(filepath, obj, callback) {
    this.writeFile(filepath, JSON.stringify(obj, null, 4), "utf8", err => {
      callback(err);
    });
  }
}

module.exports = fileJson;
