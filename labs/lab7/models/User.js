const mongoose = require("mongoose");
const { Schema } = mongoose;

const UserSchema = new Schema({
  username: { type: String, required: true },
  role: { type: Number, default: 0 },
  fullname: { type: String, required: true },
  registeredAt: { type: Date, default: Date.now },
  avatarUrl: {
    type: String,
    default:
      "https://res.cloudinary.com/drv2azi1q/image/upload/v1542146238/avatar.png"
  },
  isDisabled: { type: Boolean, default: false },
  password: { type: String, required: true },
  email: { type: String, required: true },
  posts: [{ type: Schema.Types.ObjectId, ref: "Post" }]
});

const User = mongoose.model("User", UserSchema);

module.exports = User;
