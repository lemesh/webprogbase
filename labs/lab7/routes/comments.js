const express = require("express");
const router = express.Router();

function checkAuth(req, res, next) {
  if (!req.user) return res.redirect("/auth/login");
  next();
}

const Comment = require("../models/Comment");
const Post = require("../models/Post");

const pageInfo = {
  page: 1,
  search: ""
};

router.get("/", checkAuth, (req, res) => {
  if (Object.keys(req.query).length === 0)
    res.redirect("/comments?search=&page=1");
  else {
    let page = req.query.page;
    if (page === "Previous" && pageInfo.page !== 1) page = pageInfo.page - 1;
    if (page === "Next") page = pageInfo.page + 1;
    const search = req.query.search || null;
    const obj = {};
    if (search) obj.content = new RegExp(search, "gi");
    const par = {
      page,
      limit: 10,
      sort: { postDate: 1 }
    };
    Comment.paginate(obj, par)
      .then(data => {
        let begin = 1;
        let end = data.pages;
        if (data.pages > 5) {
          begin = data.pages - data.page > 1 ? data.page - 2 : data.pages - 4;
          end = data.pages - data.page > 1 ? data.page + 2 : data.pages;
        }
        if (data.page < 4) {
          begin = 1;
          end = data.pages > 5 ? 5 : data.pages;
        }
        const pages = [];
        for (let i = begin; i <= end; i++) {
          pages.push(
            new Object({
              num: i,
              active: i === +data.page ? "btn-primary" : "btn-link"
            })
          );
        }
        pageInfo.page = data.page;
        pageInfo.search = search;
        res.render("comments", {
          comments: data.docs,
          title: "Comments",
          pages,
          prev: +data.page === 1 ? "prev" : "",
          next: +data.page === data.pages ? "next" : "",
          last: data.pages,
          search: pageInfo.search,
          len: data.docs.length === 0 ? "len" : "",
          pagesCount: data.pages,
          user: req.user,
          commentsActive: "active"
        });
      })
      .catch(err => res.status(400).send(err.toString()));
  }
});

router.get("/new", checkAuth, (req, res) => {
  Post.find()
    .then(posts => {
      res.render("newComment", {
        title: "New comment",
        user: req.user,
        posts
      });
    })
    .catch(err => res.status(400).send(err.toString()));
});

router.post("/new", checkAuth, (req, res) => {
  Comment.create(Object.assign(req.body, { author: req.user._id }))
    .then(comment => {
      return Post.findById(comment.post).then(post => {
        post.comments.push(comment._id);
        return Post.findByIdAndUpdate(comment.post, post).then(() =>
          res.redirect(`/posts/${comment.post}`)
        );
      });
    })
    .catch(err => res.status(400).send(err.toString()));
});

router.post("/add", checkAuth, (req, res) => {
  Comment.create(Object.assign(req.body, { author: req.user._id }))
    .then(comment => {
      return Post.findById(comment.post).then(post => {
        post.comments.push(comment._id);
        return Post.findByIdAndUpdate(comment.post, post)
          .populate("author")
          .then(post => res.redirect(`/users/${post.author.username}`));
      });
    })
    .catch(err => res.status(400).send(err.toString()));
});

router.post("/delete", checkAuth, (req, res) => {
  Comment.findByIdAndRemove(req.body.comment)
    .then(() => res.redirect("/comments"))
    .catch(err => res.status(400).send(err.toString()));
});

module.exports = router;
