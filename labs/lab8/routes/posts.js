const express = require("express");
const router = express.Router();
const cloudinary = require("cloudinary");

const Post = require("../models/Post");
const User = require("../models/User");
const Comment = require("../models/Comment");

const config = require("../config/config");

cloudinary.config({
  cloud_name: config.cloudinary.cloud_name,
  api_key: config.cloudinary.api_key,
  api_secret: config.cloudinary.api_secret
});

const pageInfo = {
  page: 1,
  search: ""
};

function checkAuth(req, res, next) {
  if (!req.user) return res.redirect("/auth/login");
  next();
}

router.get("/", checkAuth, (req, res) => {
  if (Object.keys(req.query).length === 0)
    res.redirect("/posts?search=&page=1");
  else {
    let page = req.query.page;
    if (page === "Previous" && pageInfo.page !== 1) page = pageInfo.page - 1;
    if (page === "Next") page = pageInfo.page + 1;
    const search = req.query.search || null;
    const obj = {};
    if (search) obj.title = new RegExp(search, "gi");
    const par = {
      page,
      limit: 10,
      sort: { postDate: 1 }
    };
    Post.paginate(obj, par)
      .then(data => {
        let begin = 1;
        let end = data.pages;
        if (data.pages > 5) {
          begin = data.pages - data.page > 1 ? data.page - 2 : data.pages - 4;
          end = data.pages - data.page > 1 ? data.page + 2 : data.pages;
        }
        if (data.page < 4) {
          begin = 1;
          end = data.pages > 5 ? 5 : data.pages;
        }
        const pages = [];
        for (let i = begin; i <= end; i++) {
          pages.push(
            new Object({
              num: i,
              active: i === +data.page ? "btn-primary" : "btn-link"
            })
          );
        }
        pageInfo.page = data.page;
        pageInfo.search = search;
        res.render("posts", {
          posts: data.docs,
          title: "Posts",
          pages,
          prev: +data.page === 1 ? "prev" : "",
          next: +data.page === data.pages ? "next" : "",
          last: data.pages,
          search: pageInfo.search,
          len: data.docs.length === 0 ? "len" : "",
          pagesCount: data.pages,
          user: req.user,
          postsActive: "active"
        });
      })
      .catch(err => res.status(400).send(err.toString()));
  }
});

router.get("/new", checkAuth, (req, res) => {
  res.render("newPost", {
    title: "New post",
    user: req.user,
    postsActive: "active"
  });
});

router.post("/new", checkAuth, (req, res) => {
  let fileObject;
  if (req.files) fileObject = req.files.photo;
  let fileBuffer;
  if (fileObject) fileBuffer = fileObject.data;
  cloudinary.v2.uploader
    .upload_stream({ resource_type: "raw" }, function(error, result) {
      Post.create(
        Object.assign(req.body, {
          author: req.user._id,
          photo: error ? "" : result.url
        })
      )
        .then(post => {
          return User.findById(req.user._id).then(user => {
            if (user) {
              user.posts.push(post._id);
              return User.findOneAndUpdate({ _id: user._id }, user).then(() =>
                res.redirect(`/posts/${post._id}`)
              );
            }
          });
        })
        .catch(err => res.status(400).send(err.toString()));
    })
    .end(fileBuffer);
});

router.post("/delete", checkAuth, (req, res) => {
  const { post } = req.body;
  Comment.find({ post })
    .then(comments => {
      const promises = [];
      comments.map(x => promises.push(Comment.findByIdAndRemove(x._id)));
      promises.push(Post.findByIdAndRemove(req.body.post));
      return Promise.all(promises);
    })
    .then(() => res.redirect("/posts"))
    .catch(err => res.status(400).send(err.toString()));
});

router.post("/edit/:post", checkAuth, (req, res) => {
  let fileObject;
  if (req.files) fileObject = req.files.photo;
  let fileBuffer;
  if (fileObject) fileBuffer = fileObject.data;
  cloudinary.v2.uploader
    .upload_stream({ resource_type: "raw" }, function(error, result) {
      Post.findByIdAndUpdate(
        req.params.post,
        Object.assign(req.body, { photo: error ? "" : result.url })
      )
        .then(post => res.redirect(`/posts/${post._id}`))
        .catch(err => res.status(400).send(err.toString()));
    })
    .end(fileBuffer);
});

router.get("/:post", checkAuth, (req, res) => {
  Post.findById(req.params.post)
    .populate({
      path: "comments",
      populate: { path: "author" }
    })
    .exec()
    .then(post => {
      res.render(
        "post",
        Object.assign(post, { title: post.title, user: req.user })
      );
    })
    .catch(err => res.status(400).send(err.toString()));
});

module.exports = router;
