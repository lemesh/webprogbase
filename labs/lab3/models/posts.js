const fs = require('fs');
const path = require('path');
const filePath = path.resolve(__dirname, '../data/') + '/posts.json';
const jsonParsed = JSON.parse(fs.readFileSync(filePath));
let posts = jsonParsed.items;
let nextId = jsonParsed.nextId;

const update = () => {
    fs.writeFileSync(filePath, JSON.stringify({nextId: nextId, items: posts}, null, 4));
};

class Post {
    constructor(id, content, title, userId, likes, postDate) {
        this.id = id;
        this.content = content;
        this.title = title;
        this.userId = userId;
        this.postDate = postDate;
        this.likes = likes;
    }

    static insert(x) {
        posts.push(new Post(nextId++, x.content, x.title, x.userId, 0, new Date().toISOString()));
        update();
        return posts.length - 1;
    }

    static getAll() {
        return posts;
    }

    static getById(id) {
        return posts.find(x => x.id === id);
    }


    static update(x) {
        let post = posts.find(el => el.id === x.id);
        post = x.post;
        update();
    }

    static delete(id) {
        const index = posts.indexOf(posts.find(x => x.id === id));
        posts = posts.slice(0, index).concat(posts.slice(index + 1));
        update();
    }
}

module.exports = Post;