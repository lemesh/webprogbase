const User = require('./models/user');
const Post = require('./models/posts');

const path = require('path');
const express = require('express');
const consolidate = require('consolidate');
const mustache = require('mustache-express');
const fs = require('fs');

const app = express();
const port = process.env.PORT || 3030;
const css = 'stylesheets/style.css';
const logo = 'images/eyes.png';

app.use(express.static('public'));
app.engine('mst', mustache(path.join(__dirname, 'views/partials')));
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'mst');

app.get('/', (req, res) => {
    const userData = {
        title: "Look At!",
        cssPath: css,
        logo,
        dot: '',
        homeAct: 'active',
    };
    res.render('index', userData);
});

app.get('/users', (req, res) => {
    const userData = {
        users: User.getAll(),
        title: "Users",
        cssPath: css,
        logo,
        dot: '',
        usAct: 'active',
    }
    res.render('users', userData);
});

app.get('/users/:id', (req, res) => {
    const user = User.getById(parseInt(req.params.id));
    if (user) {
        const userData = Object.assign(user, {
            title: `${user.fullname}`,
            cssPath: `../${css}`,
            logo: `../${logo}`,
            dot: '.',
            usAct: 'active',
        });
        res.render('user', userData);
    } else {
        const userData = {
            title: '404 Error',
            cssPath: ''
        };
        res.render('special/404', userData);
    }   
});

app.get('/posts', (req, res) => {
    const userData = {
        posts: Post.getAll(),
        title: 'Posts',
        cssPath: css,
        logo,
        dot: '',
        postAct: 'active',
    }
    res.render('posts', userData);
});

app.get('/posts/:id', (req, res) => {
    const post = Post.getById(parseInt(req.params.id));
    if (post) {
        const userData = Object.assign(post, {
            title: `${post.title}`,
            cssPath: `../${css}`,
            logo: `../${logo}`,
            dot: '.',
            postAct: 'active',
        });
        res.render('post', userData);
    } else {
        const userData = {
            title: '404 Error',
            cssPath: ''
        };
        res.render('special/404', userData);
    }
});

app.get('/about', (req, res) => {
    const userData = {
        title: 'About',
        cssPath: css,
        logo,
        dot: '',
        aboutAct: 'active'
    };
    res.render('about', userData);
});

app.get('/api/users', (req, res) => {
    res.send(User.getAll());
});

app.get('/api/users/:id', (req, res) => {
    const user = User.getById(parseInt(req.params.id));
    if (user) {
        res.send(user);
    } else {
        res.status(404).send("Error 404");
    }
});

app.listen(port, () => {
    console.log(`Server started on ${port} port`);
}); 