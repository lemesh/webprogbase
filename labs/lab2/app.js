const User = require('./models/user');
const Post = require('./models/posts');
const {ServerApp} = require('webprogbase-console-view');
const {ConsoleBrowser} = require('webprogbase-console-view');
const {InputForm} = require('webprogbase-console-view');

const app = new ServerApp();
const browser = new ConsoleBrowser();

app.use("/", (req, res) => {
    let links = {
        "users": "Users menu",
        "posts": "Posts menu",
    };
    res.send("You are in main menu", links);
});

app.use("users", (req, res) => {
    let links = {
        "allUsers": "Show all users",
        "getUser": "Get user by id",
    };
    res.send("Users menu", links);
});

app.use("allUsers", (req, res) => {
    const users = User.getAll();
    if (users) {
        let usersText = "";
        for (let user of users) {
            usersText += `* ${user.id}) ${user.fullname}\r\n`;
        }
        res.send(usersText);
    } else {
        throw new Error("Users is empty");
    }
});

app.use("getUser", (req, res) => {
    let nextState = "showUser";
    let fields = {
        "id": "Enter id of user",
    };
    let form = new InputForm(nextState, fields);
    res.send("Fill the form", form);
});

app.use("showUser", (req, res) => {
    let user = User.getById(parseInt(req.data.id, 10));
    if(user) {
        res.send(`* ${user.id}) ${user.fullname}`);
    } else {
        res.redirect("getUser");
    }
});

app.use("posts", (req, res) => {
    let links = {
        "add": "Add new post",
        "allPosts": "Show all posts",
        "getPost": "Get post by id",
        "update": "Update information of post",
        "delete": "Delete post by id",
    }
    res.send("Posts menu", links);
});

app.use("delete", (req, res) => {
    let nextState = "deletePost";
    let fields = {
        "id": "Enter id of post you want to delete",
    };
    let form = new InputForm(nextState, fields);
    res.send("Fill the form", form);
});

app.use("deletePost", (req, res) => {
    Post.delete(parseInt(req.data.id));
    res.redirect("posts");
});

app.use("add", (req, res) => {
    let nextState = "addTo";
    let fields = {
        "content": "Enter content of post",
        "title": "Enter title of post",
        "userId": "Enter user id of post",
    };
    let form = new InputForm(nextState, fields);
    res.send("Fill the form", form);
});

app.use("addTo", (req, res) => {
    const data = req.data;
    const post = {
        content: data.content,
        title: data.title,
        userId: parseInt(data.userId, 10),
    };
    Post.insert(post);
    res.redirect("posts");
});

app.use("allPosts", (req, res) => {
    const posts = Post.getAll();
    if (posts) {
        let postsText = "";
        for (let post of posts) {
            postsText += `* ${post.id}) ${post.title}\r\n`;
        }
        res.send(postsText);
    } else {
        throw new Error("Posts is empty");
    }
});

app.use("getPost", (req, res) => {
    let nextState = "showPost";
    let fields = {
        "id": "Enter id of post",
    };
    let form = new InputForm(nextState, fields);
    res.send("Fill the form", form);
});

app.use("showPost", (req, res) => {
    const post = Post.getById(parseInt(req.data.id, 10));
    if (post) {
        res.send(`* ${post.id}) ${post.title}`);
    } else {
        throw new Error("Post doesn't exist");
    }
});

app.use("update", (req, res) => {
    let links = {
        "contentPost": "Update content of post",
        "titlePost": "Update title of post",
    };
    res.send("Update list", links);
});

app.use("contentPost", (req, res) => {
    let nextState = "contentUpd";
    let fields = {
        "id": "Enter id of post, you want to update",
        "content": "Enter new content",
    };
    let form = new InputForm(nextState, fields);
    res.send("Fill the form", form);
});

app.use("contentUpd", (req, res) => {
    const post = Post.getById(parseInt(req.data.id, 10));
    if (post) {
        post.content = req.data.content;
        Post.update({
            id: parseInt(req.data.id),
            data: post,
        });
        res.redirect("posts");
    } else {
        throw new Error("Post doesn't exist");
    }
});

app.use("titlePost", (req, res) => {
    let nextState = "titleUpd";
    let fields = {
        "id": "Enter id of post, you want to update",
        "title": "Enter new title",
    };
    let form = new InputForm(nextState, fields);
    res.send("Fill the form", form);
});

app.use("titleUpd", (req, res) => {
    const post = Post.getById(parseInt(req.data.id, 10));
    if (post) {
        post.title = req.data.title;
        Post.update({
            id: parseInt(req.data.id),
            data: post,
        });
        res.redirect("posts");
    } else {
        throw new Error("Post doesn't exist");
    }
});

app.listen(3000);

browser.open(3000);