const fs = require('fs');
const path = require('path');
const filePath = path.resolve(__dirname, '../data/') + '/users.json';
const jsonParsed = JSON.parse(fs.readFileSync(filePath));
const user = jsonParsed.items;
let nextId = jsonParsed.nextId;

function User (id, login, role, fullname, registeredAt, avaUrl, isDisabled) {
    this.id = id;
    this.login = login;
    this.role = role;
    this.fullname = fullname;
    this.registeredAt = registeredAt;
    this.avaUrl = avaUrl;
    this.isDisabled = isDisabled;
};

module.exports = {
    getAll: () => {
        return user;
    },
    getById: (id) => {
        return user.find(x => x.id === id);
    },
};