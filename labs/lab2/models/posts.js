const fs = require('fs');
const path = require('path');
const filePath = path.resolve(__dirname, '../data/') + '/posts.json';
const jsonParsed = JSON.parse(fs.readFileSync(filePath));
let posts = jsonParsed.items;
let nextId = jsonParsed.nextId;

const update = () => {
    fs.writeFileSync(filePath, JSON.stringify({nextId: nextId, items: posts}, null, 4));
};

function Post (id, content, title, userId, likes, postDate) {
    this.id = id;
    this.content = content;
    this.title = title;
    this.userId = userId;
    this.postDate = postDate;
    this.likes = likes;
};

module.exports = {
    insert: (x) => {
        posts.push(new Post(nextId++, x.content, x.title, x.userId, 0, new Date().toISOString()));
        update();
        return posts.length - 1;
    },
    getAll: () => {
        return posts;
    },
    getById: (id) => {
        return posts.find(x => x.id === id);
    },
    update: (x) => {
        posts.find(el => el.id === x.id);
        post = x.post;
        update();
    },
    delete: (id) => {
        posts = posts.filter(x => x.id !== id);
        update();
    }
};