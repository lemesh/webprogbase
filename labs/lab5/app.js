const express = require("express");
const busBoyBodyParser = require("busboy-body-parser");
const mustache = require("mustache-express");
const path = require("path");
const mongoose = require("mongoose");

const app = express();

const databaseUrl = "mongodb://localhost:27017/lab";
const connectionOptions = { useNewUrlParser: true };

app.use(express.static("public"));
app.use(busBoyBodyParser());
app.set("views", path.join(__dirname, "views"));
app.set("view engine", "mst");
app.engine("mst", mustache(path.join(__dirname, "views/partials")));

app.get("/", (req, res) => {
  res.render("index", { title: "Look At!" });
});

const userRouter = require("./routes/users");
app.use("/", userRouter);

const postRouter = require("./routes/posts");
app.use("/", postRouter);

app.get("/about", (req, res) => {
  res.render("about", { title: "About" });
});

const port = process.env.PORT || 3030;

mongoose
  .connect(
    databaseUrl,
    connectionOptions
  )
  .then(() => console.log(`Database connected: ${databaseUrl}`))
  .then(() =>
    app.listen(port, () => console.log(`Server started on port ${port}`))
  )
  .catch(err => console.error(`Start error: ${err}`));
