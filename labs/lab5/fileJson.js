const fs = require("fs-extra");
const path = require("path");

class fileJson {
  static readFile(filepath) {
    return fs
      .readFile(path.join(__dirname, filepath))
      .then(data => Promise.resolve(data))
      .catch(err => {
        console.log(err);
        return Promise.reject(err);
      });
  }

  static writeFile(filepath, data, encoding) {
    return fs
      .writeFile(path.join(__dirname, filepath), data, encoding)
      .then(() => Promise.resolve(true))
      .catch(err => Promise.reject(err));
  }

  static parseJSON(filepath) {
    return this.readFile(filepath)
      .then(data => {
        try {
          const obj = JSON.parse(data);
          return Promise.resolve(obj);
        } catch (e) {
          console.log(e);
          return Promise.reject(e);
        }
      })
      .catch(err => {
        console.log(err);
        return Promise.reject(err);
      });
  }

  static jsonStringifyToFile(filepath, obj) {
    return fs
      .writeFile(
        path.join(__dirname, filepath),
        JSON.stringify(obj, null, 4),
        "utf8"
      )
      .then(() => Promise.resolve(true))
      .catch(err => Promise.reject(err));
  }
}

module.exports = fileJson;
