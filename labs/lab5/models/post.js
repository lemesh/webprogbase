const mongoose = require("mongoose");
const mongoosePaginate = require("mongoose-paginate");

const Comment = require("./comment");

const PostSchema = new mongoose.Schema({
  content: {
    type: String,
    required: true
  },
  title: {
    type: String,
    required: true
  },
  userId: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "User"
  },
  likes: {
    type: Number,
    default: 0
  },
  postDate: {
    type: Date,
    default: Date.now
  },
  photoUrl: {
    type: String,
    default: ""
  },
  comments: [
    {
      type: mongoose.Schema.Types.ObjectId,
      ref: "Comment"
    }
  ]
});

PostSchema.plugin(mongoosePaginate);

const PostModel = mongoose.model("Post", PostSchema);

class Post {
  constructor(id, content, title, userId, likes, postDate, photoUrl) {
    this.id = id;
    this.content = content;
    this.title = title;
    this.userId = userId;
    this.postDate = postDate;
    this.likes = likes;
    this.photoUrl = photoUrl;
  }

  static getAll(page, search) {
    if (page || search) {
      page = parseInt(page);
      const obj = {};
      const pageN = page || 1;
      if (search) {
        obj.title = search;
      }
      return PostModel.paginate(obj, { page: pageN, limit: 10 });
    }
    return PostModel.find({});
  }

  static addComment(commentId, postId) {
    PostModel.findById(postId, "comments")
      .exec()
      .then(data => {
        data.comments.push(commentId);
        PostModel.findOneAndUpdate({ _id: postId }, { comments: data.comments })
          .exec()
          .catch(err => console.log(err));
      });
    return Promise.resolve("Hi");
  }

  static getById(id) {
    return PostModel.findById(id)
      .populate("userId")
      .populate("comments")
      .exec();
  }

  static insert(obj) {
    return PostModel.create(obj);
  }

  static delete(id) {
    PostModel.findById(id)
      .then(data => {
        const { comments } = data;
        for (let i = 0; i < comments.length; i++) {
          console.log(comments[i]);
          Comment.delete(comments[i]).then();
        }
      })
      .catch(err => console.error(err));
    return PostModel.remove({ _id: id });
  }

  static update(obj, id) {
    return PostModel.findByIdAndUpdate(id, obj);
  }
}

module.exports = Post;
