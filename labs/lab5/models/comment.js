const mongoose = require("mongoose");

const CommentSchema = new mongoose.Schema({
  post: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "Post"
  },
  userId: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "User",
    default: "5bd61b7a6b683b13f6750ccd"
  },
  content: {
    type: String,
    required: true
  },
  likes: {
    type: Number,
    default: 0
  },
  commentDate: {
    type: Date,
    default: Date.now
  },
  replies: [
    {
      type: mongoose.Schema.Types.ObjectId,
      ref: "Comment"
    }
  ]
});

const CommentModel = mongoose.model("Comment", CommentSchema);

class Comment {
  static getAll(postId) {
    return CommentModel.find({ post: postId })
      .populate("author")
      .populate("replies")
      .populate("userId")
      .exec();
  }

  static getById(commentId) {
    return CommentModel.findById(commentId)
      .populate("author")
      .populate("replies")
      .populate("userId")
      .exec();
  }

  static insert(obj) {
    return CommentModel.create(obj);
  }

  static delete(commentId) {
    return CommentModel.findByIdAndRemove(commentId);
  }

  static update(obj, id) {
    return CommentModel.findByIdAndUpdate(id, obj);
  }
}

module.exports = Comment;
