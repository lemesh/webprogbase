const fileJson = require("../fileJson");
const mongoose = require("mongoose");

const UserSchema = new mongoose.Schema({
  login: {
    type: String,
    required: true
  },
  role: {
    type: Number,
    default: 0
  },
  fullname: {
    type: String,
    required: true
  },
  registeredAt: {
    type: Date,
    default: Date.now
  },
  avaUrl: {
    type: String,
    default: ""
  },
  isDisabled: {
    type: Boolean,
    default: false
  }
});

const UserModel = mongoose.model("User", UserSchema);

class User {
  constructor(id, login, role, fullname, registeredAt, avaUrl, isDisabled) {
    this.id = id;
    this.login = login;
    this.role = role;
    this.fullname = fullname;
    this.registeredAt = registeredAt;
    this.avaUrl = avaUrl;
    this.isDisabled = isDisabled;
  }

  static getAll() {
    return UserModel.find();
  }

  static getById(id) {
    return UserModel.findById(id);
  }
}

module.exports = User;
