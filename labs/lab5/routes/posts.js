const express = require("express");
const fs = require("fs-extra");
const path = require("path");

const Post = require("../models/post");
const Comment = require("../models/comment");

const faker = require("faker");

const router = express.Router();

const pageInfo = {
  page: 1,
  search: ""
};

const start = () => {
  for (let i = 0; i < 100; i++) {
    Post.insert({
      title: faker.lorem.sentence(),
      content: faker.lorem.paragraph(),
      userId: "5bd61b7a6b683b13f6750ccd",
      photoUrl: ""
    });
  }
};

// start();

router.get("/posts", (req, res) => {
  if (Object.keys(req.query).length === 0)
    res.redirect("/posts?search=&page=1");
  else {
    let page = req.query.page;
    if (page === "Previous" && pageInfo.page !== 1) page = pageInfo.page - 1;
    if (page === "Next") page = pageInfo.page + 1;
    const search = req.query.search || null;
    Post.getAll(page, search)
      .then(data => {
        let begin = 1;
        let end = data.pages;
        if (data.pages > 5) {
          begin = data.pages - data.page > 1 ? data.page - 2 : data.pages - 4;
          end = data.pages - data.page > 1 ? data.page + 2 : data.pages;
        }
        if (data.page < 4) {
          begin = 1;
          end = data.pages > 5 ? 5 : data.pages;
        }
        const pages = [];
        for (let i = begin; i <= end; i++) {
          pages.push(
            new Object({
              num: i,
              active: i === data.page ? "btn-primary" : "btn-link"
            })
          );
        }
        pageInfo.page = data.page;
        pageInfo.search = search;
        res.render("posts", {
          posts: data.docs,
          title: "Posts",
          pages,
          prev: data.page === 1 ? "prev" : "",
          next: data.page === data.pages ? "next" : "",
          last: data.pages,
          search: pageInfo.search,
          len: data.docs.length === 0 ? "len" : "",
          pagesCount: data.pages
        });
      })
      .catch(err => res.status(500).send(err.toString()));
  }
});

router.get("/posts/new", (req, res) => {
  res.render("newPost", { title: "New post" });
});

router.get("/posts/:postId", (req, res) => {
  const postId = req.params.postId;
  if (req.query.comment) {
    Comment.insert({
      post: postId,
      content: req.query.comment
    }).then(data => {
      Post.addComment(data._id, postId).then(() => {
        res.redirect(`/posts/${postId}`);
      });
    });
  } else {
    Post.getById(postId)
      .then(data => {
        res.render("post", Object.assign(data, { title: data.title }));
      })
      .catch(err => res.status(500).send(err.toString()));
  }
});

router.post("/posts/:userId/:id", (req, res) => {
  const id = req.params.id;
  const userId = req.params.userId;
  Comment.delete(id).then(() => res.redirect(`/posts/${userId}`));
});

router.post("/posts/new", (req, res) => {
  const file = req.files.photoUrl;
  if (file) {
    fs.writeFile(
      path.join(__dirname, `../data/fs/${file.name}`),
      file.data
    ).catch(err => res.status(500).send(err.toString()));
  }
  Post.insert(
    Object.assign(req.body, { photoUrl: file ? `/files/${file.name}` : "" })
  )
    .then(data => res.redirect(`/posts/${data._id}`))
    .catch(err => res.status(500).send(err.toString()));
});

router.get("/files/:photoUrl", (req, res) => {
  let contentType = "";
  const photo = req.params.photoUrl;
  if (
    photo.indexOf(".jpeg") !== -1 ||
    photo.indexOf(".jpg") !== -1 ||
    photo.indexOf(".png") !== -1
  )
    contentType = "image/JPEG";
  fs.readFile(path.join(__dirname, `../data/fs/${photo}`)).then(data =>
    res.send(data)
  );
});

router.post("/posts/:userId", (req, res) => {
  const userId = req.params.userId;
  Post.delete(userId)
    .then(() => res.redirect("/posts"))
    .catch(err => res.status(500).send(err.toString()));
});

module.exports = router;
