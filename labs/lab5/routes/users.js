const express = require("express");

const User = require("../models/user");

const router = express.Router();

router.get("/users", (req, res) => {
  User.getAll()
    .then(data => res.render("users", { users: data, title: "Users" }))
    .catch(err => res.status(500).send(err.toString()));
});

router.get("/users/:userId", (req, res) => {
  const userId = req.params.userId;
  User.getById(userId)
    .then(data =>
      res.render("user", Object.assign(data, { title: data.fullname }))
    )
    .catch(err => res.status(500).send(err.toString()));
});

module.exports = router;
