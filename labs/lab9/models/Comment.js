const mongoose = require("mongoose");
const mongoosePaginate = require("mongoose-paginate");

const { Schema } = mongoose;

const CommentSchema = new Schema({
  post: { type: Schema.Types.ObjectId, ref: "Post", required: true },
  author: { type: Schema.Types.ObjectId, ref: "User", required: true },
  content: { type: String, required: true },
  likes: { type: Number, min: 0, default: 0 },
  commentDate: { type: Date, default: Date.now },
  replies: [{ type: Schema.Types.ObjectId, ref: "Comment" }]
});

CommentSchema.plugin(mongoosePaginate);
module.exports = Comment = mongoose.model("Comment", CommentSchema);
